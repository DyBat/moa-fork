package moa.classifiers.average;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AverageTest {
	
	Average average;
	
	@Test
	public void averageTestMethod() {
		average = new Average();
		// (2 + 2)/2 should be equal to 2
		assertEquals(2, average.findAverage(2, 2));
	}
}
